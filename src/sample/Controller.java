package sample;


import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import sample.animation.Shake;

public class Controller {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Label LabelTermOne;

    @FXML
    private TextField tfInPut1;

    @FXML
    private Label LabelTermTwo;

    @FXML
    private TextField tfInPut2;

    @FXML
    private Button ButtonAdd;

    @FXML
    private Button ButtonClear;

    @FXML
    private Label LabelTheAmount;

    @FXML
    private TextField textFileldOutPut;

    @FXML
    void add(ActionEvent event) {
        if (tfInPut1.getText().isEmpty() || tfInPut2.getText().isEmpty()) {
            Shake firstOperandAnim = new Shake(tfInPut1);
            Shake secondOperandAnim = new Shake(tfInPut2);
            firstOperandAnim.playAnimation();
            secondOperandAnim.playAnimation();
            return;
        }
        int first = Integer.parseInt(tfInPut1.getText());
        int second = Integer.parseInt(tfInPut2.getText());
        int sum = first + second;
        textFileldOutPut.setText(String.valueOf(sum));
    }

    @FXML
    void clear(ActionEvent event) {
        tfInPut1.clear();
        tfInPut2.clear();
        textFileldOutPut.clear();
    }

    @FXML
    void initialize() {

    }
}
